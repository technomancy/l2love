local planets = require "planets"
local stars = require "stars"

local star_field = stars.new(90, 10, 200)

local w,h = love.window.getMode()
local x,y,r = 0,0,0
local speed, turn_speed, top_speed = 0,480,100
local ship = love.graphics.newImage("lavspa.png")

local angle = function()
   return (math.modf(r/45) * 45) % 360
end

local move = function(r)
   local a = 0.8
   if(r == 0) then return -1,0 end
   if r == 45 then return -a,a end
   if r == 90 then return 0,1 end
   if r == 135 then return a,a end
   if r == 180 then return 1,0 end
   if r == 225 then return a,-a end
   if r == 270 then return 0,-1 end
   if r == 315 then return -a,-a end
   return 0,0
end

love.draw = function()
   stars.render(star_field, -x, -y, w, h)
   local radians = math.rad(angle())
   love.graphics.draw(ship,w/2,h/2,radians,
      nil,nil,ship:getWidth()/2,ship:getHeight()/2)
   love.graphics.translate(x,y)
   for _,p in pairs(planets) do
      love.graphics.draw(p.img,p.x,p.y)   
   end
   
   
end

love.update = function(dt)
   if(love.keyboard.isDown("up")) then
      if(speed < top_speed) then
         speed=speed-(dt*50)
      end
      local dy,dx = move(angle())
      x = x + dx * dt * speed
      y = y + dy * dt * speed
   else
      speed=0
   end
   if(love.keyboard.isDown("escape"))then
      love.event.quit()

   end
   if(love.keyboard.isDown("right"))then
      r=r+dt*turn_speed
   end
   if(love.keyboard.isDown("left"))then
      r=r-(dt*turn_speed)
   end
end