(set bombs {})
(set lume (require "lume"))
(set x 30)
(set y 573)
(set size 10)
(set speed 100)
(set bomb_img (love.graphics.newImage "bomb.png"))

(defun make-bomb ()
  (table.insert bombs {"x" x "y" y "timer" 3 "radius" 10}))

(set level 1)
(let (w (love.graphics.getWidth)
      h (love.graphics.getHeight))
  (set levels [[[0 0 10 h]
                [0 0 w 10]
                [(- w 9) 0 10 h]
                [0  (- h 9)  w 10]
                [100 100 100 9]
                [200 200 405 9]
                [100 190 19 490]
                [200 100 10 500]
                [119 49 300 10]
                [100 49 20 100]
                [700 107.1466008814 25 10]
                [541 22 10 98]
                [542 11 8 12]
                [595 200 10 290]]]))

(set walls [[[211 347 382 44]
             [401 10 12 37]
             [595 492 15 98]
             [550 122 7 75]
             [604 201 183 25]]])

(set goal {"x" 395 "y" 268 "w" 5 "h" 5})

(defun check (x1 y1 w1 h1 x2 y2 w2 h2)
  (and (< x1 (+ x2 w2))
       (< x2 (+ x1 w1))
       (< y1 (+ y2 h2))
       (< y2 (+ y1 h1))))

(defun check-wall (wall)
  (apply check (- x size) (- y size) (* 2 size) (* 2 size) wall))

(defun collided-any-wall (x y)
  (or (lume.any (. 1 levels) check-wall)
      (lume.any (. 1 walls) check-wall)))

(defun check-explode-wall (bomb wall)
  (if (check (- bomb.x bomb.radius) (- bomb.y bomb.radius)
             (* bomb.radius 2) (* bomb.radius 2)
             (. 1 wall) (. 2 wall) (. 3 wall) (. 4 wall))
      (lume.remove (. level walls) wall)))

(defun update-bomb (dt bomb)
  (set bomb.timer (- bomb.timer dt))
  (if (< bomb.timer -3)
      (lume.remove bombs bomb)
    (if (< bomb.timer 0)
        (do (set bomb.radius (+ bomb.radius (* 10 dt)))
            (lume.map (. level walls) (lume.fn check-explode-wall bomb))))))

(defun next-level ()
  (love.event.quit)) ; for now

(defun love.update (dt)
  (let (old-x x old-y y)
    (if (love.keyboard.isDown "right")
        (set x (+ x (* speed dt))))
    (if (love.keyboard.isDown "left")
        (set x (- x (* speed dt))))
    (if (love.keyboard.isDown "down")
        (set y (+ y (* speed dt))))
    (if (love.keyboard.isDown "up")
        (set y (- y (* speed dt))))
    (if (love.keyboard.isDown "escape")
        (love.event.quit))
    (if (love.keyboard.isDown "space" " ")
        (make-bomb))
    (if (collided-any-wall x y)
        (do (set x old-x)
            (set y old-y)))
    (lume.map bombs (lume.fn update-bomb dt))))

(defun draw-wall (wall)
  (apply love.graphics.rectangle "fill" wall))

(defun draw-bomb (bomb)
  (love.graphics.setColor 255 255 255)
  (if (> bomb.timer 0)
      (love.graphics.draw bomb-img bomb.x bomb.y)
    (love.graphics.circle "fill" bomb.x bomb.y bomb.radius)))

(defun love.draw ()
  (love.graphics.setColor 255 255 255)
  (lume.map (. level levels) draw-wall)
  (love.graphics.setColor 43 237 21)
  (lume.map (. level walls) draw-wall)
  (lume.map bombs draw-bomb)
  (love.graphics.setColor 142 138 3)
  (love.graphics.rectangle "fill" goal.x goal.y goal.w goal.h)
  (love.graphics.circle "fill" x y size)
  (if (check x y size size goal.x goal.y goal.w goal.h)
      (next-level)))
