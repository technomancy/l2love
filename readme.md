# l2love

[Lisp](https://github.com/meric/l2l) to [LÖVE](https://love2d.org).

For now, just take a look in `examples/maze-bomb` and do `make run`.

## Status

Very unstable; proof-of-concept.

All LÖVE functionality should be usable from lisp, but error messages
and documentation are very poor.

Copyright © 2016 Phil Hagelberg
